const path = require('path');

module.exports = {
    entry: {
        application: './app/app.js'
    },
    output: {
        path: path.resolve(__dirname, ''),
        publicPath: '/',
        filename: 'dist/bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                exclude: [/node_modules/]
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
	    {
    		test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
    		loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
	    }
        ]
    },
    plugins: [
    ]
};
