import React from 'react'
import { withRouter } from 'react-router-dom'

import Post from '../components/post'

class PostPage extends React.Component {
    render() {
        const { history } = this.props
        return (
            <div>
                <button onClick={(e) => {
                    e.stopPropagation()
                    history.goBack() }
                }>Back</button>
                <Post/>
            </div>
        )
    }
}

// Create a new component that is "connected" (to borrow redux terminology) to the router.
const PostPageWithRouter = withRouter(PostPage)

export default PostPageWithRouter
