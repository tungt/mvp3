import React from 'react'
import { withRouter } from 'react-router-dom'

import MainLayout from '../layout/main'
import Wall from '../components/wall'

class WallPage extends React.Component {
    render() {
        const { history } = this.props
        return (
            <MainLayout>
                <Wall/>
                <button onClick={() => { history.push('/login') }}>Redirect to Login</button>
            </MainLayout>
        )
    }
}

// Create a new component that is "connected" (to borrow redux terminology) to the router.
const WallPageWithRouter = withRouter(WallPage)

export default WallPageWithRouter
