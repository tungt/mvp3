import React from 'react'
import { HashRouter, Switch, Route } from 'react-router-dom'

import WallPage from './pages/wall'
import LoginPage from './pages/login'
import SignupPage from './pages/signup'
import PostPage from './pages/post'

class Routes extends React.Component {
    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route exact path='/' component={WallPage}/>
                    <Route path='/login' component={LoginPage}/>
                    <Route path='/signup' component={SignupPage}/>
                    <Route path='/post' component={PostPage}/>
                </Switch>
            </HashRouter>
        )
    }
}

export default Routes
