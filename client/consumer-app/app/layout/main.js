import React from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import Header from '../components/header'

export default class MainLayout extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <Header />
                    <div>{this.props.children}</div>
                </div>
            </MuiThemeProvider>
        )
    }
}
