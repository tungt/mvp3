import React from 'react'

import MainLayout from '../layout/main'

export default class Signup extends React.Component {
    render() {
        return (
            <MainLayout>
                <h3>Sign Up</h3>
            </MainLayout>
        )
    }
}
