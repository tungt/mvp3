import React from 'react'
import { Link } from 'react-router-dom'

export default () => (
    <header>
        <Link to='/'>Wall</Link>&nbsp;|&nbsp;
        <Link to='/login'>Log in</Link>&nbsp;|&nbsp;
        <Link to='/signup'>Sign up</Link>&nbsp;|&nbsp;
        <Link to='/post'>Post</Link>
    </header>
)
