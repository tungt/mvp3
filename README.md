# Contributors:
- Cuong Tran
- Tung Tran

# Directories:
## client
- Single Page Apps (SPA)

## common
- Shared across Client, Server, Mobile and Web App.

## db
- Database

## deployment
- Artifacts to be deployed

## doc
- Documentation

## server
- Node.js Server Web App

## thirdparty
- External and vendor libraries
